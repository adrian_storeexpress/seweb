@extends('layouts.applogin')

@section('content')

<div class="login-clean">
    <form method="POST" action="{{ route('register') }}">
        @csrf
            <h2 class="sr-only">Login Form</h2><a class="btn btn-primary btn-block" role="button" href="/"
                style="background-color: rgb(255,255,255);"><img
                    class="rounded-circle img-fluid border d-flex d-md-flex justify-content-center m-auto justify-content-md-center align-items-md-center"
                    src="assets/img/file.jpeg" width="180"></a>
            <div class="illustration">
                <h2 class="text-center h3" style="color: rgb(0,0,0);">Crear una cuenta</h2>
            </div>
            <div class="form-group">
                <h6 class="text-left" style="color: rgb(0,0,0);">Nombre(s)</h6>
                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <h6 class="text-left" style="color: rgb(0,0,0);">Apellido(s)</h6>
                <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" required autocomplete="apellido" autofocus>
                @error('apellido')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <h6 class="text-left" style="color: rgb(0,0,0);">Número de celular</h6>
                <input id="celular" type="text" class="form-control @error('celular') is-invalid @enderror" name="celular" value="{{ old('celular') }}" required autocomplete="celular">
                @error('celular')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <h6 class="text-left" style="color: rgb(0,0,0);">N° de CI/DNI </h6>
                <input id="ci" type="text" class="form-control @error('ci') is-invalid @enderror" name="ci" value="{{ old('ci') }}" required autocomplete="ci">
            </div>
            <h6 class="text-left" style="color: rgb(0,0,0);">Contraseña</h6>
            <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <h6 class="text-left" style="color: rgb(0,0,0);">Escribe la contraseña otra vez</h6>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            
        </div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit"
                    style="background-color: rgb(31,139,255);">Confirmar</button></div><small
                class="form-text text-muted" style="font-size: 11px;"><br><a class="text-primary forgot"
                    href="{{route('login')}}">¿Ya tienes tú cuenta?&nbsp;</a><br>Al Continuar, aceptas las <a
                    href="https://bootstrapstudio.io/releases/app/4.3.7/#"><span
                        style="text-decoration: underline;">Condiciones de Uso </span></a>y el<span
                    style="text-decoration: underline;"> </span><a
                    href="https://bootstrapstudio.io/releases/app/4.3.7/#"><span
                        style="text-decoration: underline;">Aviso de Privacidad </span></a>de Store
                Expres<br><br></small>
        </form>
    </div>
@endsection
