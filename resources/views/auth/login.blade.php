@extends('layouts.applogin')
 
@section('content')
<div class="login-clean">
    <form method="POST" action="{{ route('login') }}">
    @csrf
        <h2 class="sr-only">Login Form</h2>
        <a class="btn btn-primary btn-block mt-0" role="button" href="{{route('home')}}" style="background-color: rgb(255,255,255);">
            <img class="rounded-circle img-fluid border d-flex d-md-flex justify-content-center m-auto justify-content-md-center align-items-md-center" src="assets/img/file.jpeg" width="180">
        </a>
        <div class="illustration">
            <h2 class="text-left text-dark">Iniciar sesión</h2>
        </div>
        <div class="form-group">
            <h6 class="text-left text-dark">Correo Electrónico</h6>
            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback text-dark" role="alert">
                    <strong>{{ $message }}<a href="{{route('register')}}" class="text-primary">obtén una nueva.</a></strong>
                </span>
            @enderror
        </div>
        
        <h6 class="text-left text-dark">Contraseña</h6>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group text-center mx-0">
            <button class="btn btn-primary btn-block" type="submit" style="background-color: rgb(31,139,255);">Iniciar sesión</button>
            <small><a href="{{route('register')}}">Soy nuevo</a></small>
        </div>
        <small class="form-text text-muted" style="font-size: 11px;">
            <br>
            @if (Route::has('password.request'))
                <a class="text-primary forgot" href="{{ route('password.request') }}">¿Olvidaste tú contraseña?</a>
            @endif
                <br>
            Al Continuar, aceptas las <a href="https://bootstrapstudio.io/releases/app/4.3.7/#"><span style="text-decoration: underline;">Condiciones de Uso </span></a>y el<span style="text-decoration: underline;"> </span><a href="https://bootstrapstudio.io/releases/app/4.3.7/#"><span style="text-decoration: underline;">Aviso de Privacidad </span></a>de Store Expres<br><br>
        </small>
    </form>
</div>
@endsection
