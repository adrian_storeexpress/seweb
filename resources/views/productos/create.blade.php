@extends('layouts.app')

@section('content')
    
    <div class="container bg-info text-center shadow form-inline" style="padding: 10px;">
        <h1 class="text-center col-12">Administración de Productos</h1>
    </div>

    <div class="container pt-4 pb-4 bg-light shadow">
        <div class="card card-light text-dark">
            <h4 class="card-header titulo">
                <a class="btn btn-md btn-primary" href="{{route('producto.index')}}" ><span class="fa fa-arrow-left"></span></a>
                Registrar nuevo producto
            </h4>
            <form action="{{route('producto.store')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="card-body">
                <div class="container">
                   
                        <div class="row">
                            <div class="form-group col">
                                <label for="umedida">Unidad de medida</label>
                                <select name="id_unidad_medida" id="unidad_medida" class="form-control">
                                    <option id="new_um">--AGREGAR NUEVO--</option>
                                    @foreach ($umedidas as $um)
                                    <option value="{{$um->id}}" @if ($um->unidad_medida == 'Unidad') selected @endif>{{$um->unidad_medida}}</option>
                                    @endforeach
                                </select>
                            </div>
                                
                            <div class="form-group col">
                                <label for="marca">Marca</label>
                                    <select name="id_marca" id="marca" class="form-control">
                                    <option id="new_marca">--AGREGAR NUEVO--</option>
                                    @foreach ($marcas as $m)
                                        <option value="{{$m->id}}" selected>{{$m->marca}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                                
                        <div class="form-group">
                            <label for="marca">Descripcion corta</label>
                            <textarea name="descripcion" class="form-control" rows="3" maxlength="200"></textarea>
                        </div>
            
                        <div class="form-group">
                            <label for="marca">Descripcion completa</label>
                            <textarea name="descripcion_completa" class="form-control" rows="3" maxlength="500"></textarea>
                        </div>
            
                        <div class="row">
                            <div class="form-group col">
                                <label for="cod_manual">Código manual</label>
                                <input type="text" name="cod_manual" id="cod_manual" class="form-control">
                            </div>
                            
                            <div class="form-group col">
                                <label for="cod_bar">Código barras</label>
                                <input type="text" name="cod_bar" id="cod_bar" class="form-control">
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="form-group col">
                                <label for="producto_servicio">Producto/Servicio</label>
                                <select class="form-control" name="producto_servicio">
                                    <option value="0" selected>Producto</option>
                                    <option value="1">Servicio</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="marca">Categoría</label>
                                <div class="input-group">
                                    <select name="id_categoria" id="marca" class="form-control">
                                            @foreach ($categorias as $c)
                                                <option value="{{$c->id}}">{{$c->categoria}}</option>
                                            @endforeach
                                    </select>
                                    <input type="text" class="form-control" id="cat_name">
                                    <div class="input-group-prepend">
                                            <a class="btn btn-md btn-outline-primary" id="#addC" href="#" onclick="saveCategory()">
                                                <span class="fa fa-plus-circle"></span>
                                            </a>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="form-group col">
                                <label for="tipo_iva">Tipo I.V.A.</label>
                                <select class="form-control" name="tipo_iva">
                                    <option value="10" selected>10%</option>
                                    <option value="5" >5%</option>
                                    <option value="excenta" >EXCENTA</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input name="activo" type="checkbox" aria-label="Activar/Desactiva producto">
                                    </div>
                                    <input type='text' class="form-control" readonly disabled value="Activar/Desactiva producto">
                            </div>
                            </div>
                            <div class="form-group">
                                <label>Logotipo</label>
                                <input name="image" class="form-control image" type="file" accept="image/*" onchange="loadFile(event)">
                                <img class="img-responsive" id="output" width="200"/>
                                
                                <script>
                                    var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </div>
                        </div> 
                               
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary"> <span class="fa fa-plus-circle"></span> Registrar</button>
            </div>
                 
        </form>
        </div>
    </div>


@endsection