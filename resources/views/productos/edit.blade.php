@extends('layouts.app')

@section('content')
    
<div class="container bg-info text-center shadow form-inline" style="padding: 10px;">
        <h1 class="text-center col-12">Administración de Productos</h1>
    </div>

    <div class="container pt-4 pb-4 bg-light shadow">
        <div class="card card-light text-dark">
            <h4 class="card-header titulo">
                <a class="btn btn-md btn-primary" href="{{route('producto.index')}}" ><span class="fa fa-arrow-left"></span></a>
                Modificar producto
            </h4>
            <form action="{{route('producto.update',$producto)}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card-body">
                <div class="container">
                   
                        <div class="row">
                            <div class="form-group col">
                                <label for="umedida">Unidad de medida</label>
                                <select name="id_unidad_medida" id="unidad_medida" class="form-control">
                                    <option id="new_um">--AGREGAR NUEVO--</option>
                                    @foreach ($umedidas as $um)
                                    <option value="{{$um->id}}" @if ($um->unidad_medida == $producto->unidad_medida->unidad_medida) selected @endif>{{$um->unidad_medida}}</option>
                                    @endforeach
                                </select>
                            </div>
                                
                            <div class="form-group col">
                                <label for="marca">Marca</label>
                                <select name="id_marca" id="marca" class="form-control">
                                    <option id="new_marca">--AGREGAR NUEVO--</option>
                                    @foreach ($marcas as $m)
                                        <option value="{{$m->id}}" @if ($producto->marca->marca == $m->marca) selected  @endif>{{$m->marca}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                                
                        <div class="form-group">
                            <label for="marca">Descripcion corta</label>
                        <textarea name="descripcion" class="form-control" rows="3" maxlength="200" >{{$producto->descripcion}}</textarea>
                        </div>
            
                        <div class="form-group">
                            <label for="marca">Descripcion completa</label>
                            <textarea name="descripcion_completa" class="form-control" rows="3" maxlength="500">{{$producto->descripcion_completa}}</textarea>
                        </div>
            
                        <div class="row">
                            <div class="form-group col">
                                <label for="cod_manual">Código manual</label>
                                <input type="text" name="cod_manual" id="cod_manual" class="form-control" value="{{$producto->cod_manual}}">
                            </div>
                            
                            <div class="form-group col">
                                <label for="cod_bar">Código barras</label>
                                <input type="text" name="cod_bar" id="cod_bar" class="form-control" value="{{$producto->cod_bar}}">
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="form-group col">
                                <label for="producto_servicio">Producto/Servicio</label>
                                <select class="form-control" name="producto_servicio">
                                    <option value="0" @if ($producto->producto_servicio == 0) selected @endif >Producto</option>
                                    <option value="1" @if ($producto->producto_servicio == 1) selected @endif >Servicio</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="marca">Categoría</label>
                                <select name="id_categoria" id="marca" class="form-control">
                                <option id="new_marca">--AGREGAR NUEVO--</option>
                                @foreach ($categorias as $c)
                                    <option value="{{$c->id}}" @if ($producto->categoria->categoria == $c->categoria) selected @endif>{{$c->categoria}}</option>
                                @endforeach
                            </select>
                            </div>

                            <div class="form-group col">
                                <label for="tipo_iva">Tipo I.V.A.</label>
                                <select class="form-control" name="tipo_iva">
                                    <option value="10" @if ($producto->tipo_iva == '10%') selected @endif>10%</option>
                                    <option value="5"  @if ($producto->tipo_iva == '5%') selected @endif>5%</option>
                                    <option value="excenta"  @if ($producto->tipo_iva == 'excenta') selected @endif>EXCENTA</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <input name="activo" type="checkbox" aria-label="Activar/Desactiva producto" @if ($producto->activo == 'on') checked @endif >
                                    </div>
                                    <input type='text' class="form-control" readonly disabled value="Activar/Desactiva producto">
                            </div>
                            </div>
                            <div class="form-group col-6">
                                <div class="card">
                                    <div class="card-header">
                                            <label class="h6">Logotipo</label>
                                    </div>
                                    <div class="card-body">
                                        <img class="img-responsive" id="output" width="200" src="{{asset($imagen)}}"/> 
                                    </div>
                                    <div class="card-footer">
                                        <input name="image" class="form-control image" type="file" accept="image/*" onchange="loadFile(event)" value="{{$imagen}}">
                                        {{$imagen}}
                                    </div>
                                </div>
                                
                                <script>
                                    var loadFile = function(event) {
                                        var output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </div>
                        </div> 
                               
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" onclick="$('#confirm').removeClass('fa-check');$('#confirm').addClass('fa-spinner');$('#confirm').addClass('fa-spin');"> <span id="confirm" class="fa fa-check"></span> Confirmar</button>
            </div>
                 
        </form>
        </div>
    </div>
</script>
@endsection