@extends('layouts.app')

@section('content')

    <div class="row pt-4 pb-4 bg-dark shadow">
        <div class="container">
            <h4 class="text-light titulo">
                <a class="btn btn-md btn-primary" href="#"  data-toggle="modal" data-target="#createProductModal" ><span class="fa fa-plus-circle"></span></a>
                <a class="btn btn-md btn-warning" onclick="updateTable()" href="#"><span id="refresh"  class="fa fa-refresh"></span></a> 
                :. Aministración de productos</h4>
            <hr>
            
            <div class="container" id="form-container">
                
            </div>
            
            <span class="text-small text-warning p-0 m-0" id="log"></span>
            <table class="table table-light table-striped">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Código de Barras</th>
                    <th scope="col">Descreipción Corta</th>
                    <th scope="col">Categoría</th>
                    <th scope="col">Unidad de Medida</th>
                    <th scope="col">Tipo de IVA</th>
                    <th scope="col">Activo</th>
                    <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="body">
                    
                            @foreach ($productos as $p)   
                            <tr id="t-body tb{{$p->id}}">
                                <th scope="row">{{$p->cod_bar}}</th>
                                <th scope="row">{{$p->descripcion}}</th>
                                <td>{{$p->categoria->categoria}}</td>
                                <td>{{$p->unidad_medida->unidad_medida}}</td>
                                <td>{{$p->tipo_iva}}</td>
                                <td>{{$p->activo}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-sm btn-primary" href="{{route('producto.show',$p)}}" tooltip="Ver más detalles">
                                            <span class="fa fa-eye"></span>
                                        </a>
                                        <a class="btn btn-sm btn-secondary " href="{{route('producto.edit',$p)}}" tooltip="Modificar registro">
                                            <span class="fa fa-edit"></span>
                                        </a>
                                        <form id="#destroy{{$p->id}}" method="POST" action="{{route('producto.destroy',$p)}}">

                                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                            
                                        </form>
                                        <a data-id="{{ $p->id }}" class="btn btn-sm btn-danger delete_product" tooltip="Eliminar registro"
                                                
                                                onclick="
                                                var id = $(this).data('id');
                                                    var token = $('meta[name=\'csrf-token\']').attr('content');
                                                
                                                    $.ajax(
                                                    {
                                                        url: 'producto/'+id,
                                                        type: 'DELETE',
                                                        data: {
                                                            'id': id,
                                                            '_token': token,
                                                        },
                                                        beforeSend: function(){
                                                            $('#refresh'+id).removeClass('fa-trash');
                                                            $('#refresh'+id).addClass('fa-spinner');
                                                            $('#refresh'+id).addClass('fa-spin');
                                                        },
                                                        success: function (){
                                                            updateTable();
                                                            $('#refresh'+id).removeClass('fa-spinner');
                                                            $('#refresh'+id).addClass('fa-trash');
                                                            $('#refresh'+id).removeClass('fa-spin');
                                                        }
                                                    });
                                                    "   
                                                
                                                >
                                                <span id="delete{{$p->id}}" class="fa fa-trash"></span>
                                            </a>
                                
                                    </div>
                                </td>

                        </tr>
                        @endforeach
                </tbody>
                </table>
        </div>
    </div>

    
                <!-- Modal -->
                <div class="modal fade" id="createProductModal" tabindex="-1" role="dialog" aria-labelledby="createProductModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="createProductModalTitle">Registrar nuevo producto</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        <div class="card card-light text-dark">
                                                <h4 class="card-header titulo">
                                                    Completa los datos del producto nuevo
                                                </h4>
                                                <form action="{{route('producto.store')}}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                    
                                                <div class="card-body">
                                                    <div class="container">
                                                       
                                                        <div class="row">
                                                            <div class="form-group col">
                                                                <label for="umedida">Unidad de medida</label>
                                                                <select name="id_unidad_medida" id="unidad_medida" class="form-control">
                                                                    <option id="new_um">--AGREGAR NUEVO--</option>
                                                                    @foreach ($umedidas as $um)
                                                                    <option value="{{$um->id}}" @if ($um->unidad_medida == 'Unidad') selected @endif>{{$um->unidad_medida}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                                
                                                            <div class="form-group col">
                                                                <label for="marca">Marca</label>
                                                                    <div class="input-group">
                                                                        <select name="id_marca" id="marca_select" class="form-control">
                                                                            @foreach ($marcas as $m)
                                                                                <option value="{{$m->id}}" selected>{{$m->marca}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <input type="text" class="form-control" id="marca_name">
                                                                        
                                                                        <div class="input-group-prepend">
                                                                            <a class="btn btn-md btn-outline-primary" id="#addM" href="#" onclick="addMarca(document.getElementById('marca_name').value);document.getElementById('marca_name').value =' ';">
                                                                                <span id="refresh_reg_marca" class="fa fa-plus-circle"></span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                        
                                                                </select>
                                                            </div>
                                                        </div>
                                                                
                                                        <div class="form-group">
                                                            <label for="marca">Descripcion corta</label>
                                                            <textarea name="descripcion" class="form-control" rows="3" maxlength="200"></textarea>
                                                        </div>
                                            
                                                        <div class="form-group">
                                                            <label for="marca">Descripcion completa</label>
                                                            <textarea name="descripcion_completa" class="form-control" rows="3" maxlength="500"></textarea>
                                                        </div>
                                            
                                                        <div class="row">
                                                            <div class="form-group col">
                                                                <label for="cod_manual">Código manual</label>
                                                                <input type="text" name="cod_manual" id="cod_manual" class="form-control">
                                                            </div>
                                                            
                                                            <div class="form-group col">
                                                                <label for="cod_bar">Código barras</label>
                                                                <input type="text" name="cod_bar" id="cod_bar" class="form-control">
                                                            </div>
                                                        </div>
                                            
                                                        <div class="row">
                                                            <div class="form-group col">
                                                                <label for="producto_servicio">Producto/Servicio</label>
                                                                <select class="form-control" name="producto_servicio">
                                                                    <option value="0" selected>Producto</option>
                                                                    <option value="1">Servicio</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col">
                                                                    <label for="marca">Categoría</label>
                                                                    <div class="input-group">
                                                                        <select name="id_categoria" class="form-control" id="categoria_select">
                                                                                @foreach ($categorias as $c)
                                                                                    <option value="{{$c->id}}">{{$c->categoria}}</option>
                                                                                @endforeach
                                                                        </select>
                                                                        <input type="text" class="form-control" id="cat_name">
                                                                        <div class="input-group-prepend">
                                                                                <a class="btn btn-md btn-outline-primary" id="#addC" href="#" onclick="var aa = prompt('Ingrese una breve descripción: ');addCategory(aa,document.getElementById('cat_name').value);document.getElementById('cat_name').value='';">
                                                                                    <span id="refresh_reg_cat" class="fa fa-plus-circle"></span>
                                                                                </a>
                                                                        </div>
                                                                    </div>
                                                                    
                                                            </div>
                            
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-6">
                                                                <label>Activar/Desactivar producto</label>
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">
                                                                        <input name="activo" type="checkbox" aria-label="Activar/Desactivar">
                                                                    </div>
                                                                    <input type='text' class="form-control" readonly disabled value="Activar/Desactiva producto">
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="form-group col-6">
                                                                <label for="tipo_iva">Tipo I.V.A.</label>
                                                                <select class="form-control" name="tipo_iva">
                                                                    <option value="10" selected>10%</option>
                                                                    <option value="5" >5%</option>
                                                                    <option value="excenta" >EXCENTA</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            
                                                            <div class="form-group">
                                                                <label>Logotipo</label>
                                                                <input name="image" class="form-control image" type="file" accept="image/*" onchange="loadFile(event)">
                                                                <img class="img-responsive" id="output" width="200"/>
                                                                
                                                                <script>
                                                                    var loadFile = function(event) {
                                                                        var output = document.getElementById('output');
                                                                        output.src = URL.createObjectURL(event.target.files[0]);
                                                                    };
                                                                </script>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <button type="submit" class="btn btn-primary"> <span class="fa fa-plus-circle"></span> Registrar</button>
                                                </div>
                                                     
                                            </form>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    
    <!-- MODAL DE REGISTRO DE CATEGORÍA -->
    <div class="modal fade" id="regCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrar nueva categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                ...
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">
                    <span class="fa fa-plus-circle"></span> Registrar
                </button>
                </div>
            </div>
            </div>
        </div>
        <!-- MODAL DE REGISTRO DE CATEGORÍA -->
@endsection