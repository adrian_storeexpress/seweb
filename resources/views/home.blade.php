@extends('layouts.app') @section('content')



</div>

<div id="carouselExampleInterval" class="carousel slide z-depth-1-half" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active" data-interval="7">
            <img src="{{asset('img/headers/cabecera1.png')}}" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item" data-interval="7">
            <img src="{{asset('img/headers/cabecera2.png')}}" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item" data-interval="7">
            <img src="{{asset('img/headers/cabecera3.png')}}" class="d-block w-100" alt="...">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/bs-animation.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
<script src="{{asset('assets/js/Simple-Slider.js')}}"></script>

@if($pref_comercio == null)
    <script>
        $(document).ready(function() {
            $('#exampleModalLong').modal('show');
        });
    </script>
@else
    <h1 class="text-center m-0 p-4 bg-primary text-light" style=" font-family:-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif ">{{$pref_comercio->nombre}}</h1>

    <?php $x = 1; $y = 1; ?> 
    @while ($y < 3) 
        <div class="card-group  col-sm-12  mx-auto">
        @while ($x < 5) 
            <div class="card m-1 shadow">
                <img data-toggle="modal" data-target="#exampleModalScrollable{{$x+$y}}" src="https://www.deporsempre.com/wp-content/uploads/2018/10/WhatsApp-Image-2018-10-07-at-23.37.36.jpeg" class="card-img-top" alt="...">

                <!-- Modal -->
                <div class="modal fade" id="exampleModalScrollable{{$x+$y}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle{{$x+$y}}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalScrollableTitle">Producto: {{$x+$y}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempora, nam magnam, dicta fugiat unde natus distinctio explicabo, necessitatibus nisi illum optio nihil? Perferendis suscipit non minima ducimus, libero labore itaque.
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <form class="input-group col-4 col-sm-8">
                                    <input type="number" value="1" data-decimals="2" min="1" max="1000" step="1" class="form-control">
                                    <a href="#" class="btn btn-secondary shadow rounded-0 ">
                                        <span class="fa fa-shopping-cart" style="font-size: 1.3em;"></span>
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-toggle="modal" data-target="#exampleModalScrollable{{$x+$y}}" class="card-body">
                    <h5 class="card-title">Producto: {{($x*$y)-1}}</h5>
                    <p class="card-text">additional content.</p>
                </div>
                <div class="card-footer">
                    <span class="text-muted">
                        <form class="input-group">
                            <input type="number" value="1" data-decimals="2" min="1" max="1000" step="1" class="form-control">
                                <a href="#" class="btn btn-secondary shadow rounded-0 ">
                                    <span class="fa fa-shopping-cart" style="font-size: 1.3em;"></span>
                                </a>
                        </form>
                    </span>
                </div>
            </div>
            <?php  $x = $x +1; ?> 
        @endwhile
        </div>
        <?php $y = $y +1;$x=1; ?> 
    @endwhile
@endif

<div class="contenedor fixed-bottom" style="">
    <button class="botonF1">
        <span class="fa fa-shopping-cart"></span>
    </button>
    <span class="btn btna botonF4">
        <span class="fa fa-shopping-cart"></span> 0 artículos
    </span>
    <span class="btn btna botonF3">
        <span class="fa fa-check"></span> Confirmar pedido
    </span>
    <a class="btn btna botonF2">
        <span class="fa fa-recycle"></span> Vaciar Carrito
    </a>
</div>
@endsection