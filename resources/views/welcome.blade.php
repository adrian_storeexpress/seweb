@extends('layouts.app')

@section('content')
<div class="header-blue pt-4">
    <div class="form-group" style="font-size: 4px;">
        <img class="img-fluid d-md-flex m-auto justify-content-md-center align-items-md-center" src="{{asset('img/logos/logo2x.png')}}" width="500" data-aos="zoom-in">
        @guest
        <a class="btn btn-light text-center d-flex d-xl-flex justify-content-center mx-auto justify-content-xl-center align-items-xl-center" role="button" href="{{route('login')}}" style="background-color: rgb(0,123,255);padding: 0px;color: rgb(255,255,255);height: 34px;width: 93px;margin: 15px;">Identifícate</a>

        <a class=" shadow btn btn-danger text-danger text-center d-flex d-xl-flex justify-content-center mx-auto justify-content-xl-center" role="button" href="{{route('register')}}" style="background-color: rgb(255,255,255);padding: 0px;margin: 15px;height: 31px;width: 288px;">
            ¿Eres nuevo? Empieza aquí
        </a>
        @else
        <div class="container text-center p-5">
            <a href="{{route('home')}}" class="text-center btn btn-lg btn-primary shadow">¡COMPRAR AHORA!</a>
        </div>
        @endguest
    </div>
        
    <div>
        <div class="container p-5 mt-0 bg-white ml-0 mr-0 col-12 mb-0">
            <div class="row">
                <div class="col-md-12 col-xl-11">
                    <h2 class="text-center"><strong>¡Tus compras sin moverte de tu casa!</strong></h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3 col-xl-3 offset-xl-0 mx-auto"><img
                        class="rounded-circle img-fluid d-xl-flex mx-auto align-items-xl-center"
                        src="assets/img/17274.svg"
                        style="height: 91px;background-position: top;margin: 18px;background-repeat: repeat-x;">
                    <h4 style="color: rgb(21,232,67);"><strong>Envío por 3000 Gs.</strong></h4>
                    <h5>Elige la hora de entrega por 3000 Gs. o tenlo en casa en en minutos por 5000 Gs</h5>
                </div>
                <div class="col-md-3 mx-auto">
                    <figure class="figure"></figure>
                    <figure class="figure"></figure>
                    <figure class="figure"></figure>
                    <figure class="figure"></figure><img class="img-fluid d-xl-flex mx-auto align-items-xl-center"
                        src="assets/img/oficial%20oficial.png" width="100">
                    <h4 style="color: rgb(21,232,67);"><strong>Servicio Personalizado</strong></h4>
                    <h5>Un servicio hecho a la medida de nuestros clientes más exigentes.</h5>
                </div>
                <div class="col-md-3 col-xl-3 text-sm-center mx-auto">
                    <figure class="figure"></figure>
                    <figure class="figure"></figure>
                    <figure class="figure"></figure>
                    <figure class="figure"></figure><img class="img-fluid d-xl-flex mx-auto align-items-xl-center"
                        src="assets/img/oficial%20confianza.png" width="160">
                    <h4 style="color: rgb(21,232,67);"><strong>Productos de Calidad</strong></h4>
                    <h5>Elige entre una de nuestras tiendas online para hacer tus compras.</h5>
                </div>
            </div>
        </div>
    </div>
</div>
   
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-animation.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
@endsection