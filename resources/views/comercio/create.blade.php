@extends('layouts.app')

@section('content')
<div class="container bg-light text-center shadow" style="padding: 10px;">
    <h1 style="color: rgb(104,105,106);">¡¡ANTES DE CONTINUAR ASEGÚRESE DE TENER LOS SIGUIENTES DATOS!!</h1>
</div>
<div class="row pt-4 pb-4 bg-info shadow">
    <div class="col">
        <div class="row" style="padding-left: 10px;padding-right: 10px;">
            <div class="col">
                <div class="container text-center"><i class="fa fa-address-card" style="font-size: 5em;color: white;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Dirección comercial&nbsp;</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="container text-center"><i class="fa fa-phone"
                        style="font-size: 5em;color: white;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Número de contacto</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="container text-center" style="color: white;"><i class="fa fa-id-card-o"
                        style="font-size: 5em;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Detalles de identidad</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container  bg-light" style="padding: 50px;">
    <h4 style="color: rgb(150,5,5);">Completa los datos solicitados:</h4>
    <form method="POST" action="{{ route('comercio.store') }}">
        @csrf
        <div class="form-row">
            <div class="col">
                <div class="form-group"><label for="departamento">Departamento:</label>
                    <select id="depselect" class="form-control selectpicker" 
                        name="departamento"  data-live-search="true">
                        <optgroup label="Departamentos del Paraguay">
                            @if($departamentos != null)
                            <?php $ii = 0; ?>
                                @foreach ($departamentos as $d)
                                    <option value="{{$d->departamento}}" @if($ii == 0) selected @endif>{{$d->departamento}}</option>
                                <?php $ii = 1; ?>
                                @endforeach    
                            @endif

                        </optgroup>
                    </select></div>
            </div>
            <div class="col">
                <div class="form-group"><label for="ciudad">Ciudad:</label>
                    <select class="form-control selectpicker" id="select-country"  data-live-search="true" name="ciudad">
                        <optgroup label="Departamentos del Paraguay">
                                @if($localidades != null)
                                <?php $ii = 0;$ss = ""; ?>
                                    @foreach ($localidades as $l)
                                        <option data-tokens="{{strtolower($l->localidad)}}" value="{{$l->localidad}}" @if($ii == 0) selected @endif>{{$l->localidad}}</option>
                                    <?php $ii = 1; ?>
                                    @endforeach    
                                @endif
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group"><label for="barrio">Barrio:</label><input required  class="form-control" type="text"
                        name="barrio"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-4">
                <div class="form-group"><label for="tipo_empresa">Tipo de empresa:</label><select class="form-control"
                        name="tipo_empresa">
                        <optgroup label="Seleccionar el tipo de su empresa">
                            <option value="Unipersonal" selected="">Unipersonal</option>
                            <option value="S.A." >S.A.</option>
                            <option value="S.R.L.">S.R.L.</option>
                            <option value="LTDA">LTDA</option>
                        </optgroup>
                    </select></div>
                <label for="nombre">Nombre de la empresa:</label><input required  name="nombre" class="form-control" type="text" placeholder="Ej: StoreExpress">
            </div>
            <div class="col">
                <h5>Rubros:</h5>
                <div class="form-row">
                    <div class="col" style="">
                            <select multiple="multiple" id="my-select" name="rubros[]" class="form-control">
                                @if($rubros != null)
                                <?php $ii = 0;$ss = ""; ?>
                                    @foreach ($rubros as $l)
                                        <option data-tokens="{{strtolower($l->rubro)}}" value="{{$l->id}}" @if($ii == 0) selected @endif>{{$l->rubro}}</option>
                                    <?php $ii = 1; ?>
                                    @endforeach    
                                @endif
                            </select>
                    </div>
                </div>
            </div>

            <div class="col">
                <h5>Logotipo:</h5>
                <div class="form-row">
                    <div class="col">
						<div class="form-group">
							<input class="form-control" type="file" accept="image/*" onchange="loadFile(event)" >
							<img class="img-responsive" id="output" style="width: 150px; height: 150px;"/>
							
							<script>
								var loadFile = function(event) {
									var output = document.getElementById('output');
									output.src = URL.createObjectURL(event.target.files[0]);
								};
							</script>
						</div>
					</div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
        <div class="col">
            <div class="form-group"><label>Celular:</label><input required  type="tel" name="celular" placeholder="Teléfono o celular" class="form-control"></div>
        </div>
        <div class="col">
            <div class="form-group"><label>R.U.C.:</label>
                <input required  type="text" class="form-control" name="ruc">
            </div>
        </div>
        <div class="col">
            <div class="form-check mb-2">
                <input required  class="form-check-input" type="checkbox" id="formCheck-1" name="terminos">
                <label class="form-check-label" for="formCheck-1">Acepto los términos y condiciones.</label>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Registrar</button>
        </div>
    </div>
</form>
    
</div>

@endsection