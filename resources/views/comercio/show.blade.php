@extends('layouts.app')
@section('content')

<div class="container bg-light text-center shadow" style="padding: 10px;">
    <h1 style="color: rgb(104,105,106);">PANEL DE ADMINISTRACIÓN - {{$admin_comercio->nombre}}</h1>

</div>
<div class="row pt-4 pb-4 bg-info shadow col-11 mx-auto">
    <div class="col ">
        <div class="row" style="padding-left: 10px;padding-right: 10px;">
            <div class="col">
                <div class="container text-center"><i class="fa fa-list" style="font-size: 5em;color: white;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Inventario</h4>
                    <h4 class="text-center text-light">0</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="container text-center"><i class="fa fa-usd" style="font-size: 5em;color: white;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Ventas</h4>
                    <h4 class="text-center text-light">$0</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="container text-center" style="color: white;"><i class="fa fa-truck"
                        style="font-size: 5em;"></i></div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="container">
                    <h4 class="text-center text-light">Pedidos</h4>
                    <h4 class="text-center text-light">0</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container  bg-light" style="padding: 50px;">

    <!-- Aviso de comercio no verificado -->
    @if($admin_comercio->estado == 'verificacion')
    <div class="alert alert-warning alert-dismissible fade show radius-0 border-dark" role="alert">
        En el proceso de verificación no es posible modificar ciertos datos de tu comercio. Si te haz equivocado contactar con <strong><a href="https://api.whatsapp.com/send?phone=595971308551&text=Modificacion%20de%20datos%20del%20comercio%20<?php echo str_replace('.','',str_replace(' ','%20',$admin_comercio->nombre)) ?>">Atención al Cliente</a></strong>.  

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <!-- FIN Aviso de comercio no verificado -->

    <h3>Información general de {{$admin_comercio->nombre}}</h3>
    <hr>
    <div class="container">
        <form>
            <div class="form-row">
                <div class="col-8">
                    <div class="form-row">
                        <div class="col"><label>Nombre</label>
                            <div class="form-group"><input type="text"value="{{$admin_comercio->nombre}}" 
                                    class="form-control"  @if($admin_comercio->estado == 'verificacion')  name="nombre"  readonly @endif /></div>
                        </div>
                        <div class="col">
                            <div class="form-group"><label>R.U.C.</label><input type="text"
                                    value="{{$admin_comercio->ruc}}" class="form-control " @if($admin_comercio->estado == 'verificacion')  name="ruc" readonly @endif /></div>
                        </div>
                        <div class="col">
                            <div class="form-group"><label>Dirección</label><input type="text" name="direccion"
                                    value="{{$admin_comercio->direccion}}" class="form-control" /></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group"><label>Tipo de Empresa</label><input type="text" name="tipo_empresa"
                                    value="{{$admin_comercio->tipo_empresa}}" class="form-control" /></div>
                        </div>
                        <div class="col">
                            <div class="form-group"><label>Celular/Teléfono</label><input type="text" name="celular"
                                    value="{{$admin_comercio->celular}}" class="form-control" /></div>
                        </div>
                        <div class="col">
                            <div class="form-group"><label>Correo electrónico</label><input type="text" name="email"
                                    value="{{$admin_comercio->email}}" class="form-control" /></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group"><label>Estado</label><input type="text"
                                    value="{{$admin_comercio->estado}}" class="form-control" readonly/></div>
                        </div>
                        <div class="col">
                            <div class="form-group"><label>Google Maps</label><input type="text" name="email"
                                    value="{{$admin_comercio->google_maps}}" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Logotipo</label>
                        <input class="form-control" type="file" accept="image/*" onchange="loadFile(event)">
                        <img class="img-responsive" id="output" width="200"/>
                        
                        <script>
                            var loadFile = function(event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    
                </div>                  
            </div>
        </form>
    </div>


</div>

@endsection