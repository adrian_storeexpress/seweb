<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'StoreExpress') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/ionicons.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="{{asset('assets/css/Article-List.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Contact-Form-Clean.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Footer-Basic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Footer-Clean.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Footer-Dark.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Header-Blue-1.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Header-Blue-2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Header-Blue.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Highlight-Clean.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/Login-Form-Clean.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Login-Form-Dark.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Navigation-with-Button-1.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Navigation-with-Button.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Navigation-with-Search.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Projects-Horizontal.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/Simple-Slider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
</head>
<body>

    <div id="app">
        <main class="py-0">
            @yield('content')
        </main>
    </div>

    <div class="footer-basic">
        <footer>
            <div class="social"><a href="https://www.facebook.com/SExpressPY/?hc_ref=ART9fM7lBTUECeC2spVcTWydCCiBbf0zUxhgs3CUcLqHdToTGKIBMkKKWM5v54ayCEs&amp;fref=nf&amp;__xts__[0]=68.ARD3TOOhWWmPKWuxIBHMkngCa-hAvGdeCP1ooPn3_6pMUrOoj2x7ugkgAtNWXywoCNOhLV_XiBMQA0tWX7-RfAgnLqvWUw8D96CJpUBdTZ2fewqmfQjKfee7Nn3GCrX9mWvVsjj1nYAad5zfaw2Ki8DNXvv-Z8iRftmpLE4W1rMkBduxZ-BMN2dRvlFJkrRKDlZd2Ig0fnANHHWkjiWqOxMIh6tHRlrSITyvkY9FCydooihAEeZqTGATiga47xqgC1922ba9cNsMxfM8jthwxxEcZwFUojlYki9vrk8BpW7BCaRkqEh-Tbgzq8HmSTnWEvwcm-5i8BQlfC85jmDWD-Q&amp;__tn__=kC-R"><i class="icon ion-social-facebook"></i></a>
                <a href="#"><i class="icon ion-social-twitter"></i></a><a href="https://www.instagram.com/store_expres/?hl=es-la"><i class="icon ion-social-instagram"></i></a></div>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#">Inicio</a></li>
                    <li class="list-inline-item"><a href="#">Empresas</a></li>
                    <li class="list-inline-item"><a href="#">Acerca de Store Express</a></li>
                    <li class="list-inline-item"><a href="#">Condiciones de Uso</a></li>
                    <li class="list-inline-item"><a href="#">Aviso de Privacidad</a></li>
                </ul>
            <p class="copyright">Store Express.com © 2019<br></p>
        </footer>
    </div>
</body>
</html>
