<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head >
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <title>Inicio - TiendaExpress</title>
    <link rel="stylesheet" href="{{asset('assets/css/Footer-Dark.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/fonts/ionicons.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">   
     <link href="path/to/multiselect.css" media="screen" rel="stylesheet" type="text/css">

    
</head>

<body>
<div style="background: linear-gradient(135deg, #172a74, #21a9af);
background-color: #184e8e;">
    <nav style="background: linear-gradient(45deg, #184e8e 0%, rgba(150,150,255,1) 100%);" class="navbar navbar-dark navbar-expand-md  shadow-sm">
        <div class="container-fluid">
            <a href="#" class="navbar-brand">
                <img src="{{asset('/assets/img/logo%202%20blanco.png')}}" width="190" class="img-fluid" />
            </a>
            <button data-toggle="collapse" data-target="#navcol" class="navbar-toggler">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol">
                <ul class="nav navbar-nav">
                    <li class="nav-item dropdown" id="dropdowna">
                        <a data-toggle="#dropdowna" aria-expanded="false" href="#" class="dropdown-toggle nav-link">Tiendas</a>
                        <div role="menu" class="dropdown-menu">
                            
                        </div>
                    </li>
                    
                    @if($admin_comercio != null)
                    <li role="presentation" class="nav-item">
                        <a href="{{route('comercio.create')}}" class="nav-link">Vender</a>
                    </li>
                    @endif

                    <li class="nav-item dropdown">
                        <a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle nav-link">Ayuda</a>
                        <div role="menu" class="dropdown-menu">
                            <a role="presentation" href="#" class="dropdown-item">Preguntas frecuentes</a>
                            <hr>    
                            <a role="presentation" href="#" class="dropdown-item">Contacto</a>
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav ml-auto">
                    @guest
                    <li role="presentation" class="nav-item">
                        <a href="{{route('login')}}" class="nav-link">Ingresar <i class="fa fa-sign-in"></i></a>
                    </li>
                    @else
                    <li class="dropdown nav-link">
                            <a class="dropdown-toggle nav-link text-light" data-toggle="dropdown"
                                aria-expanded="false" href="#"><!-- CORTAR CADENA -->Hola, {{ Auth::user()->nombre}} </a>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" role="presentation" href="#">
                                    <span class="fa fa-user"> Mi cuenta</span>
                                </a>
                                <a class="dropdown-item" role="presentation" href="#">
                                    <span class="fa fa-cog"> Configuraciones</span>
                                </a>
                                
                                <a class="dropdown-item" role="presentation" href="#">
                                    <span class="fa fa-shopping-cart"> Mi Carrito</span>
                                </a>
                                <a class="dropdown-item" role="presentation" href="#">
                                    <span class="fa fa-list"> Mis pedidos</span>
                                </a>
                                @if($admin_comercio != null)
                                    <a class="dropdown-item btn-block btn btn-primary bg-primary rounded-0 shadow text-light" role="presentation" href="{{route('comercio.show',$admin_comercio)}}">
                                        <span class="fa fa-list"> Administrar {{$admin_comercio->nombre}}</span>
                                    </a>
                                @endif
                                <a class="dropdown-item rounded-0" role="presentation"  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span class="fa fa-sign-out"> Cerrar sesión</span>
                                </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endguest
                    <li role="presentation" class="nav-item">
                        <a href="{{route('home')}}" class="nav-link text-light bg-primary border rounded shadow">Comienza tu compra</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="mb-0">
            @yield('content')
    </main>
</div>
    <div class="footer-dark mb-0 mt-0" style="font-size: 17px;background-color: rgb(49,55,58);height: 164;">
        <footer>
            <div class="container mb0">
                <div class="row text-center">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Conócenos</h3>
                        <ul>
                            <li><a href="#" style="font-size: 17px;">Trabaja en Store Express</a></li>
                            <li><a href="#" style="font-size: 17px;">Acerca de Store Express</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Los comercios en StoreExpress</h3>
                        <ul>
                            <li style="font-size: 17px;"><a href="{{route('comercio.create')}}">Sugerir un comercio</a></li>
                            <li style="font-size: 17px;"><a href="{{route('comercio.create')}}">Vende en Store Express</a></li>
                            <li style="font-size: 17px;"><a href="#">Anuncia tus productos</a></li>
                            <li></li>
                        </ul>
                    </div>
                    <div class="col-md-6 item text">
                        <h3>¿Cómo funciona?</h3>
                        <p style="font-size: 17px;"><strong>Añade productos a tu carrito desde la App o la web de Store express, elegí una hora de entrega y nosotros nos encargamos del resto.</strong></p>
                    </div>
                    <div class="col item social">
                        <a href="https://www.facebook.com/SExpressPY/"><i class="icon ion-social-facebook"></i></a>
                        <a href="https://twitter.com/Storeexpress1">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="https://www.instagram.com/store_expres/?hl=es-la"><i class="icon ion-social-instagram"></i></a></div>
                </div>
                <p class="copyright">Store Express.com © 2019</p>
            </div>
        </footer>
    </div>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

<script>
$('#rubros').multiSelect()

$('.botonF1').hover(function(){
  $('.btn').addClass('animacionVer');
});
$('.contenedor').mouseleave(function(){
  $('.btn').removeClass('animacionVer');
});
</script>

    
<script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{asset('css/popper.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    
    <script>
        function updateTable(){
            
            var token = $('meta[name=\'csrf-token\']').attr('content');
                
                $.ajax(
                {
                    type: 'POST',
                    url: 'getAllProducts',
                    data: {
                        '_token': token,
                    },
                    beforeSend: function(){
                        $("#refresh").addClass('fa-spin');
                    },
                    success: function (data){                      
                        $('#body').html(data.html); 
                        
                        $("#refresh").removeClass('fa-spin');
                        
                    }
                });
        }
        function updateCategorySelect(){
            
            var token = $('meta[name=\'csrf-token\']').attr('content');
                
                $.ajax(
                {
                    type: 'POST',
                    url: '{{route("getAllCategories")}}',
                    data: { 
                        '_token': token,
                    },
                    beforeSend: function(){
                        $("#refresh_reg_cat").removeClass('fa-plus-circle');
                        $("#refresh_reg_cat").addClass('fa-spinner');
                        $("#refresh_reg_cat").addClass('fa-spin');
                    },
                    success: function (data){     
                        
                        $("#categoria_select option").remove();      
                        $("#categoria_select").html(data.html);      
                        
                        $("#refresh_reg_cat").removeClass('fa-spinner');
                        $("#refresh_reg_cat").removeClass('fa-spin');
                        $("#refresh_reg_cat").addClass('fa-plus-circle');
                        
                    }
                });
        }
        function addCategory(descripcion,nombre){
            
            var token = $('meta[name=\'csrf-token\']').attr('content');
                
                $.ajax(
                {
                    url: '{{route("categoria.store")}}',
                    type: 'POST',
                    data: {
                        'descripcion' : descripcion,
                        'categoria' : nombre,
                        '_token': token,
                    },
                    beforeSend: function(){
                        $("#refresh_reg_cat").removeClass('fa-plus-circle');
                        $("#refresh_reg_cat").addClass('fa-spinner');
                        $("#refresh_reg_cat").addClass('fa-spin');
                    },
                    success: function (data){                      
                        $("#refresh_reg_cat").removeClass('fa-spinner');
                        $("#refresh_reg_cat").removeClass('fa-spin');
                        $("#refresh_reg_cat").addClass('fa-plus-circle');
                        updateCategory Select();
                    }
                });
        }
        function addMarca(nombre){
            
            var token = $('meta[name=\'csrf-token\']').attr('content');
                
                $.ajax(
                {
                    url: '{{route("marca.store")}}',
                    type: 'POST',
                    data: {
                        'marca' : nombre,
                        '_token': token,
                    },
                    beforeSend: function(){
                        $("#refresh_reg_marca").removeClass('fa-plus-circle'); 
                        $("#refresh_reg_marca").addClass('fa-spinner');
                        $("#refresh_reg_marca").addClass('fa-spin');
                    },
                    success: function (data){                      
                        $("#refresh_reg_marca").removeClass('fa-spinner');
                        $("#refresh_reg_marca").removeClass('fa-spin');
                        $("#refresh_reg_marca").addClass('fa-plus-circle');
                        updateMarcaSelect();
                    }
                });
        }
        function updateMarcaSelect(){
            
            var token = $('meta[name=\'csrf-token\']').attr('content');
                
                $.ajax(
                {
                    type: 'POST',
                    url: '{{route("getAllMarcas")}}',
                    data: {
                        '_token': token,
                    },
                    beforeSend: function(){
                        $("#refresh_reg_marca").removeClass('fa-plus-circle');
                        $("#refresh_reg_marca").addClass('fa-spinner');
                        $("#refresh_reg_marca").addClass('fa-spin');
                    },
                    success: function (data){     
                        
                        $("#marca_select option").remove();      
                        $("#marca_select").html(data.html);      
                        
                        $("#refresh_reg_marca").removeClass('fa-spinner');
                        $("#refresh_reg_marca").removeClass('fa-spin');
                        $("#refresh_reg_marca").addClass('fa-plus-circle');
                        
                    }
                });
        }
        function deleteProduct(id){
            var token = $('meta[name=\'csrf-token\']').attr('content');
        
            $.ajax(
            {
                url: "producto/"+id,
                type: "DELETE",
                data: {
                    "id": id,
                    "_token": token,
                },
                beforeSend: function(){
                    $("#log").html("Eliminando registro...");
                    $("#log").show("slow");
                },
                success: function (){
                    updateTable();
                    $("#log").html("Registro eliminado con éxito...");
                    $("#log").hide("slow");

                
                }
            });
        }
        
    </script>
    <script src="assets/js/script.min.js"></script>

    
<!-- 
    <div class="footer-basic">
        <footer>
            <div class="social"><a href="https://www.facebook.com/SExpressPY/?hc_ref=ART9fM7lBTUECeC2spVcTWydCCiBbf0zUxhgs3CUcLqHdToTGKIBMkKKWM5v54ayCEs&amp;fref=nf&amp;__xts__[0]=68.ARD3TOOhWWmPKWuxIBHMkngCa-hAvGdeCP1ooPn3_6pMUrOoj2x7ugkgAtNWXywoCNOhLV_XiBMQA0tWX7-RfAgnLqvWUw8D96CJpUBdTZ2fewqmfQjKfee7Nn3GCrX9mWvVsjj1nYAad5zfaw2Ki8DNXvv-Z8iRftmpLE4W1rMkBduxZ-BMN2dRvlFJkrRKDlZd2Ig0fnANHHWkjiWqOxMIh6tHRlrSITyvkY9FCydooihAEeZqTGATiga47xqgC1922ba9cNsMxfM8jthwxxEcZwFUojlYki9vrk8BpW7BCaRkqEh-Tbgzq8HmSTnWEvwcm-5i8BQlfC85jmDWD-Q&amp;__tn__=kC-R"><i class="icon ion-social-facebook"></i></a>
                <a
                    href="#"><i class="icon ion-social-twitter"></i></a><a href="https://www.instagram.com/store_expres/?hl=es-la"><i class="icon ion-social-instagram"></i></a></div>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Inicio</a></li>
                <li class="list-inline-item"><a href="#">Empresas</a></li>
                <li class="list-inline-item"><a href="#">Acerca de Store Express</a></li>
                <li class="list-inline-item"><a href="#">Condiciones de Uso</a></li>
                <li class="list-inline-item"><a href="#">Aviso de Privacidad</a></li>
            </ul>
            <p class="copyright">Store Express.com © 2019<br></p>
        </footer>
    </div> -->
</body>
</html>
