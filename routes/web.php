<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@welcome')->name('welcome');   


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ciudades/search', 'CiudadesController@search')->name('ciudades.search');

Route::resource('/comercio','ComercioController');
Route::resource('/usuario','UserController');
Route::resource('/usuario/preferencias','PreferenciasUsuarioController');
Route::resource('/producto','ProductosController');
Route::resource('/categoria','CategoriasController');
Route::resource('/marca','MarcasController');

Route::post('/getAllProducts','ProductosController@getAllProducts')->name('getAllProducts');
Route::post('/getAllCategories','CategoriasController@getAllCategories')->name('getAllCategories');
Route::post('/getAllMarcas','MarcasController@getAllMarcas')->name('getAllMarcas');