<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Unidad',
            'id_unidad_referencia' => '0',
            'multiplicador' => '1',
            'activo' => 'si',
            'simbolo' => 'Un.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Gramo',
            'id_unidad_referencia' => '0',
            'multiplicador' => '1',
            'activo' => 'si',
            'simbolo' => 'g',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Centrigramos',
            'id_unidad_referencia' => '2',
            'multiplicador' => '0.01',
            'activo' => 'si',
            'simbolo' => 'Cg.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Miligramos',
            'id_unidad_referencia' => '2',
            'multiplicador' => '0.001',
            'activo' => 'si',
            'simbolo' => 'Mg.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Kilogramos',
            'id_unidad_referencia' => '2',
            'multiplicador' => '1000',
            'activo' => 'si',
            'simbolo' => 'Kg.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Litros',
            'id_unidad_referencia' => '0',
            'multiplicador' => '1',
            'activo' => 'si',
            'simbolo' => 'Lts.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Mililitros',
            'id_unidad_referencia' => '6',
            'multiplicador' => '0.001',
            'activo' => 'si',
            'simbolo' => 'Ml.',
        ]);
        DB::table('unidades_medida')->insert([
            'unidad_medida' => 'Centímetros Cúbicos',
            'id_unidad_referencia' => '6',
            'multiplicador' => '0.001',
            'activo' => 'si',
            'simbolo' => 'Cc.',
        ]);
        
        $categorias = array(
            'Teclados y Mouses', 'Impresoras y Scanners', 'Notebooks', 'Monitores', 'Accesorios de celulares', 'Celulares', 'Celulares', 'Cereales', 'Yerba Mate', 'Gaseosas', 'Lacteos', 'Electricidad', 
        ); 
        $ia = 1;
        foreach($categorias as $c){
            DB::table('categorias')->insert([
                'categoria' => $c,
                'descripcion' => '',
            ]);    
            $ia = $ia+1;
        } 

        DB::table('monedas')->insert([
            'moneda' => 'Guaraníes',
            'simbolo' => 'Gs.',
            'activo' => 'si',
            'decimales_de_redondeo' => '2',
            'id_cotizacion_actual' => '1',
        ]);
        DB::table('monedas')->insert([
            'moneda' => 'Dólares',
            'simbolo' => '$',
            'activo' => 'si',
            'decimales_de_redondeo' => '0',
            'id_cotizacion_actual' => '0',
        ]);
        

        DB::table('users')->insert([
            'nombre' => 'Adrian',
            'apellido' => 'Grahl',
            'email' => 'grahlmaciel@gmail.com',
            'celular' => '595992777467',
            'password' => bcrypt('masterkey'),
        ]);
        
        DB::table('users')->insert([
            'nombre' => 'Anibal',
            'apellido' => 'Benítez',
            'email' => 'alebenitez.01@hotmail.com',
            'celular' => '595981212530',
            'password' => bcrypt('12345678'),
        ]);
		DB::table('preferencias_usuarios')->insert([
            'user_id' => '1',
            'pref_comercio_id' => '1'
		]);
		DB::table('preferencias_usuarios')->insert([
            'user_id' => '2',
            'pref_comercio_id' => '1'
		]);
        DB::table('paises')->insert([
            'pais' => 'Paraguay',
            'activo' => 'si'
        ]);

        $rubros = array(
            'Electrónica y Oficina', 'Moda y Belleza', 'Joyería','Herramientas y Ferretería','Bebé y Juguetería','Jardín y Hogar','Farmacia','Librería','Articulos de Librería','Artículos para mascotas','Accesorios automotríces','Accesorios de motos','Bebidas','Comidas','Supermercado','Minimercado','Accesorios de Deporte','Carnicería','Panadería','Confitería'
        );
        $ia = 1;
        foreach($rubros as $d){
           
            DB::table('rubros')->insert([
                'rubro' => $d,
                'activo' => 'si',
            ]);  
            $ia = $ia+1;
        }
        $marcas = array(
            'Adidas','Nike','UnderArmour','Campesino','Kurupí','HP',
            'Samsung','Huawei','Apple','Xiaomi','Motorola','Toshiba','Panasonic','Pilsen','Brahma','Norte','Tommy Hilfiger','U.S. Polo Assn','VIP','Casio','Citizen','Q&Q','Anita','Mickey','Dove','Fossil',
        );
        
        $ia = 1;
        foreach($marcas as $d){ 
            DB::table('marcas')->insert([
                'id_pais' => 1,
                'marca' => $d,
                'activo' => 'si',
            ]);  
            $ia = $ia+1;
        }

        $departamentos = array(
            'Central','Alto Paraná','Amambay','Alto Paraguay','Boquerón','Caaguazú','Caazapá','Canindeyú','Concepción','Cordillera','Guairá','Itapúa','Misiones','Ñeembucú','Paraguarí','Presidente Hayes','San Pedro',
        );
       
        $localidades = array(
            [
                'Asunción',
                'Luquue',
                'San Lorenzo',
                'Capiatá',
                'Lambaré',
                'Fernando de la Mora',
                'Limpio',
                'Ñemby',
                'Itauguá',
                'Mariano Roque Alonso',
                'Itá',
                'Villa Elisa',
                'Areguá',
                'San Antonio',
                'Julián Augusto Saldíva',
                'Ypané',
                'Villeta',
                'Guarambaré',
                'Ypacaraí',
                'Nueva Italia',
            ],
            [
                'Ciudad del Este',
                'Presidente Franco',
                'Minga Guazú',
                'Hernandarias',
                'Itakyry',
                'Santa Rita',
                'Juan Emilio OLeary',
                'Doctor Juan León Mallorquín',
                'Minga Porá',
                'San Alberto',
                'Yguazú',
                'San Cristóbal',
                'Los Cedrales',
                'Ñacunday',
                'Doctor Raúl Peña',
                'Mbaracayú',
                'Tavapy',
                'Santa Rosa del Monday',
                'Iruña',
                'Naranjal',
                'Domingo Martínez de Irala',
                'Santa Fe del Paraná',
            ],
            [
                'Pedro Juan Caballero',
                'Capitán Bado',
                'Bella Vista Norte',
                'Zanja Pytá',
                'Karapaí',
            ],
            [
                'Puerto Casado Capitán Carmelo Peralta',
                'Fuerte Olimpo',
                'Bahía Negra',
            ],
            [
                'Mariscal José Félix Estigarribia',
                'Filadelfia',
                'Loma Plata',
            ],
            [
                'Caaguazú',
                'Coronel Oviedo',
                'Doctor Juan Eulogio Estigarribia',
                'Yhú',
                'Repatriación',
                'Doctor Juan Manuel Frutos',
                'San José de los Arroyo',
                'San Joaquín',
                'Raúl Arsenio Oviedo',
                'Carayaó',
                'Tembiaporá',
                'Santa Rosa del Mbutuy',
                'Vaquería',
                'José Domingo Ocampos',
                'Tres de Febrero',
                'R. I. Tres Corrales',
                'Doctor Cecilio Báez',
                'Mariscal Francisco Solano López',
                'Simón Bolívar',
                'La Pastora',
                'Nueva Toledo',
                'Nueva Londres',
            ],
            [
                'San Juan Nepomuceno',
                'Aba',
                'Caazapá',
                'Yuty',
                'Tres de Mayo',
                'Tavaí',
                'Fulgencio Yegros',
                'General Higinio Morínigo',
                'Buena Vista',
                'Doctor Moisés Santiago Berton',
                'Maciel',
            ],
            [
                'Curuguaty',
                'Salto del Guairá',
                'Yasy Cañy',
                'Villa Ygatimí',
                'Ybyrarobaná',
                'Nueva Esperanza',
                'General Francisco Caballero Álvarez',
                'Yby Pytá ',
                'Corpus Christi',
                'La Paloma del Espíritu Santo',
                'Katueté',
                'Ypejhú',
                'Itanará',
                'Maracaná ',
                'Puerto Adela',
            ],
            [
                'Concepción',
                'Horqueta',
                'Yby Yaú',
                'Loreto ',
                'Belén',
                'San Lázaro',
                'Azotey',
                'Sargento José Félix López',
                'San Alfredo',
                'Paso Barreto',
                'San Carlos del Apa',
                'Arroyito',
            ],
            [
                'Caacupé ',
                'Tobatí',
                'Piribebuy',
                'Arroyos y Esteros',
                'Eusebio Ayala',
                'Emboscada',
                'Atyrá ',
                'Altos',
                'Caraguatay',
                'San Bernardino',
                'Itacurubí de la Cordillera',
                'Isla Pucú',
                'Valenzuela',
                'Juan de Mena',
                'Primero de Marzo ',
                'Santa Elena ',
                'San José Obrero ',
                'Mbocayaty del Yhaguy ',
                'Nueva Colombia ',
                'Loma Grande ',
            ],
            [
                'Villarrica',
                'Independencia',
                'Paso Yobái',
                'Capitán Mauricio José Troche',
                'Borja',
                'Iturbe',
                'Mbocayaty del Guairá',
                'General Eugenio Alejandrino Garay',
                'Itapé',
                'José A. Fassardi',
                'Coronel Martínez',
                'Félix Pérez Cardozo',
                'Natalicio Talavera',
                'Yataity del Guairá',
                'Ñumí',
                'Tebicuary',
                'San Salvador',
                'Doctor Bottrell',
            ],
            [
                'Encarnación',
                'Cambyretá',
                'San Pedro del Paraná',
                'Tomás Romero Pereira',
                'Edelira',
                'San Rafael del Paraná',
                'Natalio',
                'Coronel José Félix Bogado',
                'Carlos Antonio López',
                'Alto Verá',
                'Obligado',
                'Itapúa Poty',
                'Mayor Julio Dionisio Otaño',
                'Hohenau',
                'Capitán Meza',
                'Bella Vista',
                'Capitán Miranda',
                'Yatytay',
                'General Artigas',
                'Fram',
                'San Cosme y Damián',
                'San Juan del Paraná',
                'Trinidad',
                'Pirapó',
                'Carmen del Paraná',
                'General Delgado',
                'Nueva Alborada',
                'Jesús',
                'José Leandro Oviedo',
                'La Paz',
            ],
            [
                'San Ignacio Guazú',
                'San Juan Bautista',
                'Ayolas',
                'Santa Rosa de Lima',
                'Santa María de Fe',
                'Santiago',
                'San Miguel',
                'San Patricio',
                'Villa Florida',
                'Yabebyry',
            ],
            [
                'Pilar',
                'Alberdi',
                'San Juan Bautista del Ñeembucú',
                'Cerrito',
                'Mayor José de Jesús Martínez',
                'General José Eduvigis Díaz',
                'Tacuaras',
                'Villa Oliva',
                'Laureles',
                'Humaitá',
                'Isla Umbú',
                'Villalbín',
                'Guazú Cuá',
                'Paso de Patria',
                'Desmochados',
                'Villa Franca',
            ],
            [
                'Carapeguá',
                'Yaguarón',
                'Ybycuí',
                'Paraguarí',
                'Quiindy',
                'Pirayú',
                'Acahay',
                'Mbuyapey',
                'San Roque González de Santa Cruz',
                'Escobar',
                'Caapucú',
                'Quyquyhó',
                'Ybytymí',
                'General Bernardino Caballero',
                'Sapucai',
                'La Colmena',
                'Tebicuarymí',
                'María Antonia',
            ],
            [
                'Villa Hayes',
                'Teniente Primero Manuel Irala Fernández',
                'Benjamín Aceval',
                'Puerto Pinasco',
                'Nanawa',
                'José Falcón',
                'Teniente Esteban Martínez',
                'General José María Bruguez',
            ],
            [
                'San Estanislao',
                'Capiibary',
                'Santa Rosa del Aguaray',
                'San Pedro de Ycuamandiyú',
                'Guayaibí',
                'Choré',
                'General Isidoro Resquín',
                'Liberación',
                'General Elizardo Aquino',
                'Tacuatí',
                'Yrybucuá',
                'Yataity del Norte',
                'Lima',
                'Itacurubí del Rosario',
                'Villa del Rosario',
                '25 de Diciembre',
                'Unión',
                'Nueva Germania',
                'Antequera',
                'San Pablo',
                'San Vicente Pancholo'
            ],
        );
        foreach ($departamentos as $d){
            DB::table('departamentos')->insert([
                'id_pais' => '1',
                'departamento' => $d,
                'activo' => 'si',
                'cod_departamento' => ' ',
            ]);    
        }
        
        $ii = 1;
        foreach($localidades as $d){
            foreach($d as $l){    
                DB::table('localidades')->insert([
                    'id_departamento' => $ii,
                    'localidad' => $l,
                    'activo' => 'si',
                    'cod_localidad' => ' ',
                ]);    
            }
            $ii = $ii+1;
        }
    }
}
