<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_comercio');
            $table->integer('id_producto');
            $table->integer('stock');
            $table->string('estado',10);
            $table->decimal('costo',10,3)->nullable();
            $table->decimal('precio_1',10,3);
            $table->decimal('precio_2',10,3);
            $table->decimal('precio_3',10,3);
            $table->decimal('precio_4',10,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
