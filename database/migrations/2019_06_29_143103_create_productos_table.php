<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_categoria');
            $table->integer('id_unidad_medida');
            $table->integer('id_marca');
            $table->string('cod_manual',10)->nullable();
            $table->string('cod_bar',20)->nullable();
            $table->string('descripcion',200)->nullable();
            $table->string('descripcion_completa',500);
            $table->integer('producto_servicio');
            $table->integer('tipo_iva');
            $table->string('nombre_imagen',50);
            $table->string('activo',10);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
