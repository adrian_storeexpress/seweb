<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesMedidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades_medida', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unidad_medida',35)->nullable();
            $table->integer('id_unidad_referencia')->nullable();
            $table->decimal('multiplicador',10,3)->nullable();
            $table->string('activo',5)->nullable();
            $table->string('simbolo',8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades_medida');
    }
}
