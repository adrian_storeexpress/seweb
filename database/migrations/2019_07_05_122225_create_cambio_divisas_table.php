<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambioDivisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cambio_divisas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_cotiza_salida');
            $table->integer('id_cotiza_entrada');
            $table->integer('id_puesto');
            $table->integer('id_banco');
            $table->timestamp('fecha_hora');
            $table->decimal('monto_salida',10,3);
            $table->decimal('monto_entrada',10,3);
            $table->decimal('monto_base',10,3);
            $table->string('num_factura',20)->nullable();
            $table->string('imputar_caja',5)->nullable();
            $table->string('user_name',25)->nullable();
            $table->string('estado',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cambio_divisas');
    }
}
