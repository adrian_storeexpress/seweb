<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_moneda');
            $table->timestamp('fecha_cotizacion');
            $table->decimal('cotizacion',10,3);
            $table->string('operador',1)->nullable();
            $table->decimal('multi_a_base',10,3)->nullable();
            $table->decimal('multi_de_base',10,3)->nullable();
            $table->decimal('cotiza_compra',10,3);
            $table->integer('id_cambio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
