<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    
    protected $fillable = [
        'user_id', 'comercio_id','fecha_hora','monto','estado' 
    ];
}
