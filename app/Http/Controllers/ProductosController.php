<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Auth;
use App\Producto;
use App\UnidadMedida;
use App\Marca;
use App\Categoria;
use App\Comercio;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::get('*');
        $admin_comercio = Comercio::find(Auth::user()->comercio->id);
        $umedidas = UnidadMedida::get('*');
        $marcas = Marca::get('*');
        $categorias = Categoria::get('*');
        $admin_comercio = Comercio::find(Auth::user()->comercio->id);
        return view('productos/index',['productos' => $productos,'umedidas' => $umedidas,'marcas' => $marcas,'categorias' => $categorias,'admin_comercio' =>$admin_comercio]);
    }

    public function getAllProducts(){
        $productos = Producto::get('*');
        $html = "";
        foreach($productos as $p){
            $html .= '<tr id="t-body tb'.$p->id.'">
                        <th scope="row">'.$p->cod_bar.'</th>
                        <th scope="row">'.$p->descripcion.'</th>
                        <td>'.$p->categoria->categoria.'</td>
                        <td>'.$p->unidad_medida->unidad_medida.'</td>
                        <td>'.$p->tipo_iva.'</td>
                        <td>'.$p->activo.'</td>';
            $html .= '<td>
            <div class="btn-group">
                <a class="btn btn-sm btn-primary" href="{{route(\'producto.show\','.$p->id.')}}" tooltip="Ver más detalles">
                    <span class="fa fa-eye"></span>
                </a>
                <a class="btn btn-sm btn-secondary " href="{{route(\'producto.edit\','.$p->id.')}}" tooltip="Modificar registro">
                    <span class="fa fa-edit"></span>
                </a>
                <form id="#destroy'.$p->id.'" method="POST" action="{{route(\'producto.destroy\','.$p->id.')}}">

                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    
                </form>
                <a data-id="'.$p->id.'" class="btn btn-sm btn-danger delete_product" tooltip="Eliminar registro"
                        onclick="var id = $(this).data(\'id\'); deleteProduct(id);">
                        <span id="delete'.$p->id.'" class="fa fa-trash"></span>
                    </a>
        
            </div>
        </td>';
        }
        return response()->json([
            'html' => $html
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $umedidas = UnidadMedida::get('*');
        $marcas = Marca::get('*');
        $categorias = Categoria::get('*');
        $admin_comercio = Comercio::find(Auth::user()->comercio->id);
        return view('productos/create',['umedidas' => $umedidas,'marcas' => $marcas,'categorias' => $categorias,'admin_comercio' =>$admin_comercio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $producto = new Producto($request->all());
           
        $producto->nombre_imagen = "vacio";
        $producto->save();
        
        if($request->file('image') != null ){  
             $producto->nombre_imagen = $producto->id.'__'.$request->file('image')->getClientOriginalName();
             $path = $request->file('image')->storeAs('public/images',$producto->id.'__'.$request->file('image')->getClientOriginalName());
         }  
         
        $producto->save();

        return redirect('/producto')->with('success', '¡El producto se registró satisfactoriamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        $umedidas = UnidadMedida::get('*');
        $marcas = Marca::get('*');
        $categorias = Categoria::get('*');
        $admin_comercio = Comercio::find(Auth::user()->comercio->id);
        $image = Storage::url('public/images/'.$producto->nombre_imagen);
        return view('productos/edit',['imagen'=>$image,'producto' => $producto,'umedidas' => $umedidas,'marcas' => $marcas,'categorias' => $categorias,'admin_comercio' =>$admin_comercio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $file = $producto->id.'__'.$request->file('image')->getClientOriginalName();
        if($request->file('image') != null ){
            Storage::delete(['public/images/'.$producto->nombre_imagen]);
            $producto->nombre_imagen = $file;
            $path = $request->file('image')->storeAs('public/images',$file);
        }
        $producto->update($request->all());
        return redirect('producto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Producto::find($id)->delete($id);
        return response()->json([
            'success' => 'Registro eliminado con éxito.'
        ]);
    }
}
