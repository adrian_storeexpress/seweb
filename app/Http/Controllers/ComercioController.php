<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\Departamento;
use App\Localidad;
use App\ComercioRubro;
use Auth;

use Illuminate\Support\Facades\DB;
class ComercioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth'); // Obliga inicio de sesión
    }
    public function index()
    {
        
        return view('comercio/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = DB::table('departamentos')->orderBy('departamento', 'asc')->get();
        $localidades = DB::table('localidades')->orderBy('localidad', 'asc')->get();
        $rubros = DB::table('rubros')->orderBy('rubro', 'asc')->get();
    
        if(Auth::user()->comercio != null){
            return redirect('/home')->with('error','Ya tienes una empresa registrada!');
        }
        return view('comercio/create',['departamentos' => $departamentos,'rubros' => $rubros,'localidades' => $localidades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
    
        $c = new Comercio();
        $c->user_id = Auth::user()->id;
        $c->nombre = $request['nombre'];
        $c->ruc = $request['ruc'];
        $c->celular = $request['celular'];
        $c->departamento = $request['departamento'];
        $c->ciudad = $request['ciudad'];
        $c->barrio = $request['barrio'];
        $c->tipo_empresa = $request['tipo_empresa'];
        $c->save();

        foreach($request['rubros'] as $rubro){
            $cr = new ComercioRubro();
            $cr->id_comercio = $c->id;
            $cr->id_rubro = $rubro;
            $cr->save();
        }
        
        return redirect('/comercio/'.$c->id)->with('success', '¡Tu comercio ha sido registrado satisfactoriamente!');
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin_comercio = Comercio::find($id);
        if($admin_comercio != null){
            if(Auth::user()->comercio == $admin_comercio){   
                return view('comercio/show',['admin_comercio' => $admin_comercio]);
            }else{
                return redirect()->action('HomeController@index');
            }
        }else{
            return redirect()->action('HomeController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('comercio/edit');        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Comercio::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(($comercio = Comercio::find($id) != null)){
            foreach ($comercio->rubros as $relacion_con_rubro) {
                $relacion_con_rubro->destroy();
            }
            foreach ($comercio->stock as $stock) {
                $stock->destroy();
            }
            
            $comercio->destroy();
    
        }
    }
}
