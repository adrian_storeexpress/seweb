<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\PreferenciasUsuario;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$comercios = Comercio::get('*');
		$admin_comercio = null;
		$pref_comercio = null;
        $pref = null;
        
        if(isset(Auth::user()->preferencias)){
            $pref = Auth::user()->preferencias; 
        }

        if($pref == null){
            $pref = new PreferenciasUsuario();
            $pref->pref_comercio_id = 0;
            $pref->user_id = Auth::user()->id;
            $pref->save();
        }
        
        $pref_comercio = Comercio::find($pref->pref_comercio_id);
        
		
        if(Auth::user()->comercio != null){
            $admin_comercio = Auth::user()->comercio;
        }
        return view('home',['admin_comercio'=>$admin_comercio,'comercio'=>$comercios,'pref_comercio' => $pref_comercio]);
    }
    public function welcome()
    {
		$admin_comercio = null;

        
        if(Auth::user() != null){
            if(Auth::user()->comercio != null){
                $admin_comercio = Auth::user()->comercio;
            }
        }
        return view('welcome',['admin_comercio'=>$admin_comercio]);
    }
	
}
