<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria; 

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    
    public function getAllCategories(){
        $cat = Categoria::get('*');
        $html = "";
        foreach($cat as $c){
            $html .= '<option value="'.$c->id.'">'.$c->categoria.'</option>';
        }
        return response()->json([
            'hola' => 'hola',
            'html' => $html
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = new Categoria();
        $c->categoria = $request['categoria'];
        $c->descripcion = $request['descripcion'];
        $c->save();
        $html = '<option value="'.$c->id.'">'.$c->categoria.'</option>';
        return response()->json(["html"=>$html,'id'=>$c->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
