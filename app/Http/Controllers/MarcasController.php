<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
class MarcasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $m = new Marca();
        $m->id_pais = 1;
        $m->marca = $request['marca'];
        $m->activo = 'si';
        $m->save();
        $html = '<option value="'.$m->id.'">'.$m->marca.'</option>';
        return response()->json(["html"=>$html,'id'=>$c->id]);
    }

    
    public function getAllMarcas(){
        $marcas = Marca::get('*');
        $html = "";
        foreach($marcas as $m){
            $html .= '<option value="'.$m->id.'">'.$m->marca.'</option>';
        }
        return response()->json([
            'hola' => 'hola',
            'html' => $html
        ]);
    }
}
