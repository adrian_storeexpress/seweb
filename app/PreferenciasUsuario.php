<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class PreferenciasUsuario extends Model
{
    protected $fillable = [
        'user_id','pref_comercio_id'
    ];

    
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
