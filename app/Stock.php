<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comercio;

class Stock extends Model
{
    protected $table = 'stock';

    protected $fillable = [
       'id_comercio','id_producto','stock','estado','costo','precio_1','precio_2','precio_3','precio_4' 
    ];

    public function comercio(){
        return $this->belongsTo(Comercio::class,'id_comercio','id');
    }
    
    public function producto(){
        return $this->belongsTo(Producto::class,'id_producto','id');
    }

    public function costoTotal(){
        return $this->stock * $this->costo;
    }
    

}
