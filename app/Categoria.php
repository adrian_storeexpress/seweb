<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Producto;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $fillable = [
        'moneda','descripcion' 
    ];

    public function productos(){
        return $this->hasMany(Productos::class,'id','id_categoria');
    }
}
