<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComercioRubro extends Model
{
    protected $table = 'comercio_rubro';

    protected $fillable = [
        'id_comercio','id_rubro', 
    ];
}
