<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Producto;
use App\Stock;
class Comercio extends Model
{
    protected $fillable = [
        'user_id', 'nombre', 'ruc', 'direccion', 'celular', 'departamento', 'ciudad', 'barrio','tipo_empresa','email','estado','activo','google_maps', 
    ];

    public function usuario(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function stock(){
        return $this->hasMany(Stock::class,'id','id_comercio');
    }
    public function rubros(){
        return $this->hasMany(ComercioRubro::class,'id','id_comercio');
    }
}
