<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $table = "unidades_medida";
    protected $fillable = [
        'unidad_medida', 'id_unidad_referencia', 'multiplicador', 'activo', 'simbolo', 
    ];
}
