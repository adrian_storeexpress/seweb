<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetalleCarrito;

class Carrito extends Model
{
    protected $table = "carritos";

    protected $fillable = [
      'user_id','monto', 
    ];

    public function detalles(){
      return $this->hasMany(DetalleCarrito::class,'id_carrito','id');
   }
    
}
