<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Localidad;
class Departamento extends Model
{
    protected $fillable = [
        'id_pais','departamento','activo','cod_departamento', 
    ];

    public function localidades(){
        return $this->hasMany(Localidad::class,'id_departamento','id');
    }
}
