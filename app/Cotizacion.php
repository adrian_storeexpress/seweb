<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Moneda;

class Cotizacion extends Model
{
    public function moneda(){
        return $this->belongsTo(Moneda::class,'id_moneda','id');
    }
}
