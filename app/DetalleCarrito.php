<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Carrito;
use App\Producto;

class DetalleCarrito extends Model
{
    protected $table = "detalle_carritos";

    protected $fillable = [
      'id_carrito','id_producto','cantidad','precio','subtotal' 
    ];
    
    public function carrito(){
        return $this->belongsTo(Carrito::class,'id_carrito','id');
    }

    public function producto(){
        return $this->belongsTo(Producto::class,'id_producto','id');
    }
}
