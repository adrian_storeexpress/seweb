<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Departamento;
class Localidad extends Model
{
    protected $table = 'localidades';

    protected $fillable = [
        'id_pais', 'localidad', 'activo', 'cod_localidad', 
    ];

    public function departamento(){
        return $this->belongsTo(Departamento::class,'id_departamento','id');
    }
}
