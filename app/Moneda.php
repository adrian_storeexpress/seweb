<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cotizacion;

class Moneda extends Model
{
    protected $table = 'monedas';

    protected $fillable = [
        'moneda','simbolo','activo','decimales_de_redondeo','id_cotizacion_actual' 
    ];

    public function cotizaciones(){
        return $this->hasMany(Cotizacion::class,'id_cotizacion_actual','id');
    }
}
