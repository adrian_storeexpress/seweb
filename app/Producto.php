<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UnidadMedida;
use App\Categoria;
use App\Marca;

class Producto extends Model
{
    protected $fillable = [
        'id_unidad_medida','id_categoria' ,'id_marca', 'cod_manual', 'cod_bar', 'descripcion', 'descripcion_completa', 'producto_servicio', 'tipo_iva','nombre_imagen','activo', 
    ];


    public function unidad_medida(){
        return $this->belongsTo(UnidadMedida::class,'id_unidad_medida','id');
    }
    
    public function categoria(){
        return $this->belongsTo(Categoria::class,'id_categoria','id');
    }
    public function marca(){
        return $this->belongsTo(Marca::class,'id_marca','id');
    }
    
}
