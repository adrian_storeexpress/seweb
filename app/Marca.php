<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pais;

class Marca extends Model
{
  protected $table = 'marcas';

  protected $fillable = [
    'id_pais','marca','activo', 
  ];

  public function pais(){
    return $this->belongsTo(Pais::class,'id_pais','id');
  }

}
